﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Reflection;

namespace kage
{
    class Program
    {
        #region Simulation Parameters

        public static RandomMT rand; // random number generator

        // general parameters
        public static int RUNS = 0;
        public static int TIME = 0;
        public static int N0 = 0; // initial population
        public static int N_MAX = 0; // maximum population

        // mutation
        public static double PBm_I = 0.0; // probability to have beneficial mutations affecting intrinsic fitness
        public static double PDm_I = 0.0; // probability to have dentrimental mutations affecting intrinsic fitness
        public static double PBm_E = 0.0; // probability to have beneficial mutations affecting extrinsic fitness
        public static double PDm_E = 0.0; // probability to have dentrimental mutations affecting extrinsic fitness
        public static double MI = 0.0; // mutation step (intrinsic)
        public static double ME = 0.0; // mutation step (extrinsic)

        // fitness
        public static double FE = 1.0; // initial extrinsic fitness
        public static double FI_A = 0.0; // intrinsic fitness "a"
        public static double FI_B = 0.0; // intrinsic fitness "b"

        // reproduction
        public static int R = 8; // reproduction age
        public static int OFFSPRING = 1; // number of offspring
        public static bool GENETIC_DRIFT = false; // if false animal produces exactly the number of offspring, if true it produces from 0 to OFFSPRING children 

        // environment and aging
        public static double ENV = 0; // environmental pressure
        public static string ENV_FUNCTION = "default"; // environment function
        public static string AGE_FUNCTION = "default"; // aging function

        // environment check
        public static int T = 2; // the number of "hits" an animal can take before dying (at H == T the animal dies)
        public static double PI = 1.0; // the probability that at each time step an animal is "checked" for intrinsic fitness
        public static double PE = 0.0; // the probability that at each time step an animal is "checked" for extrinsic fitness
        public static double PD = 0.0; // the percentage of animals removed at each time step due to random events
        public static bool POPULATION_BOTTLE = false;

        // various
        public static int SEED = 1;

        // non controlled constants
        public static int MAX_AGE = 1000000; // the maximum age
        public static int MIN_OFFSPRING = 0; // the minimum number of offspring
        public static bool A_NEG = false; // is it allowed for a parameter to be negative?
        public static bool B_CONST = true; // is b parameter constant?
        
        public static bool ENV_COMPATIBLE = false; // the check for environment is made according to the compatibility with current value 
        public static double ENV_PERIOD = 10.0; // controls the periodicity for the enviroment sine and cosine functions

        public static int BOTTLE_TIME = 1000; // the time when the catastrophic event takes place

        #endregion

        #region Simulation Results
        
        public static double[] Nt; // population array
        public static double[,] Pa; // age distribution (first dimension is age, second dimension is time)
        public static double[] Fia_av; // Average Fi_a; 
        public static double[] Fib_av; // Average Fi_b;
        public static double[] Fe_av; // Average Fe;
        public static double[] Age_av; // Average Age; * Age_av[0] = average age of death
        public static double[] Fia_std; // Standard deviation Fi_a; 
        public static double[] Fib_std; // Standard deviation Average Fi_b;
        public static double[] Fe_std; // Standard deviation Average Fe;
        public static double[] Age_std; // Standard deviation Average Age;

        #endregion

        static void Main(string[] args)
        {
            InitializeProgram(args); // input data and thread priority initialization

            // results folder
            string fld = "t" + TIME + "_r" + RUNS + "_No" + N0 + "_Nx" + N_MAX + "_Pbmi" + PBm_I + "_Pdmi" + PDm_I + "_Pbme" + PBm_E + "_Pdme" + PDm_E
                + "_MI" + MI + "_ME" + ME + "_Fe" + FE + "_Fia" + FI_A + "_Fib" + FI_B + "_Pi" + PI + "_Pe" + PE + "_Pd" + PD + "_R" + R + "_T" + T;
            Directory.CreateDirectory(fld); fld = fld + "\\";
            StreamWriter cws = new StreamWriter(fld + "SIM_PROGRESS.txt"); // output stream for simulations progress

            Console.WriteLine("Simulation Started");
            // RUNS START
            for (int i = 1; i <= RUNS; i++)
            {
                Habitat h = new Habitat(); // create Habitat
                h.Populate(Program.N0); // place animals on habitat

                // initalize habitat variables
                h.timeAge = 0;

                // h.RandomizeIntrinsicFitness(false); // randomize intrinsic fitness (with Normal (true) or Uniform (false) Distribution)
                // h.RandomizeExtrinsicFitness(false); // randomize extrinsic fitness (with Normal (true) or Uniform (false) Distribution)
                // h.SetInitialAge(); // sets initial age of each animal to R

                Console.WriteLine("Run: " + i + "/" + RUNS);
                cws.WriteLine("Run: " + i + "/" + RUNS);

                Nt[0] += h.N; // initial population

                for (int j = 1; j <= TIME; j++)
                {
                    h.ExecuteTimeStep(j);
                    if ((j < 1000 && j % 100 == 0) || (j < 10000 && j % 1000 == 0) || (j <= 100000 && j % 10000 == 0))
                    {
                        Console.WriteLine("...at time: " + j + "/" + TIME);
                        cws.WriteLine("...at time: " + j + "/" + TIME);
                        cws.Flush();
                    }
                }
            }
            // RUNS END
            cws.Close(); // close output stream for simulations progress
            Directory.CreateDirectory(fld + "FULL_ARRAY" + "\\");

            // normalize population, and average arrays
            Normalize_Pop_Avg_Std(); // normalize population, average and standard deviation arrays
            double[,] NPa = NMath.NormalizeAgeDistribution(Pa); // normalized age distribution
            double[,] Fa = NMath.CalculateCumulativeAgeDistribution(NPa); // calculate survival probability from age distribution
            double[,] Ma = NMath.CalculateMortalityFunction(Fa); // calculate mortality function from survival probability

            // OUTPUT arrays to files

            // Population and average arrays
            ReduceAndOutputArray(Nt, fld + "Nt.txt"); ReduceAndOutputArray(Fia_av, fld + "Fia_av.txt"); ReduceAndOutputArray(Fib_av, fld + "Fib_av.txt");
            ReduceAndOutputArray(Fe_av, fld + "Fe_av.txt"); ReduceAndOutputArray(Age_av, fld + "Age_av.txt");
            OutputArray(Nt, fld + "FULL_ARRAY" + "\\" + "Nt_FULL.txt"); OutputArray(Fia_av, fld + "FULL_ARRAY" + "\\" + "Fia_av_FULL.txt"); OutputArray(Fib_av, fld + "FULL_ARRAY" + "\\" + "Fib_av_FULL.txt");
            OutputArray(Fe_av, fld + "FULL_ARRAY" + "\\" + "Fe_av_FULL.txt"); OutputArray(Age_av, fld + "FULL_ARRAY" + "\\" + "Age_av_FULL.txt");

            // Standard deviation arrays
            ReduceAndOutputArray(Fia_std, fld + "Fia_std.txt"); ReduceAndOutputArray(Fib_std, fld + "Fib_std.txt");
            ReduceAndOutputArray(Fe_std, fld + "Fe_std.txt"); ReduceAndOutputArray(Age_std, fld + "Age_std.txt");
            OutputArray(Fia_std, fld + "FULL_ARRAY" + "\\" + "Fia_std_FULL.txt"); OutputArray(Fib_std, fld + "FULL_ARRAY" + "\\" + "Fib_std_FULL.txt");
            OutputArray(Fe_std, fld + "FULL_ARRAY" + "\\" + "Fe_std_FULL.txt"); OutputArray(Age_std, fld + "FULL_ARRAY" + "\\" + "Age_std_FULL.txt");

            // Age Distribution, Survival Probability and Mortality Function arrays
            OutputArray(NPa, fld + "Pa.txt"); OutputArray(Fa, fld + "Fa.txt"); OutputArray(Ma, fld + "Ma.txt");

            // Average age at death
            StreamWriter stavdeath = new StreamWriter(fld + "AvDeath_Age.txt");
            stavdeath.WriteLine("Average Age at Death (Total): " + ((Habitat.AgeDeathAge + Habitat.DriftDeathAge + Habitat.EnvironmentDeathAge) / (Habitat.AgeDeaths + Habitat.DriftDeaths + Habitat.EnvironmentDeaths)));
            stavdeath.WriteLine("Average Age at Death from Aging: " + (Habitat.AgeDeathAge / Habitat.AgeDeaths));
            stavdeath.WriteLine("Average Age at Death from Drift: " + (Habitat.DriftDeathAge / Habitat.DriftDeaths));
            stavdeath.WriteLine("Average Age at Death from Environment: " + (Habitat.EnvironmentDeathAge / Habitat.EnvironmentDeaths));
            stavdeath.Close();

            Console.WriteLine("Simulation Finished Succesfully!");
            // END SIMULATION
        }

        // input simulation parameters
        private static void InputParameters(string file)
        {
            StreamReader str = new StreamReader(file);
            str.ReadLine();

            // general parameters
            RUNS = int.Parse(ReadNextParameter(str));
            TIME = int.Parse(ReadNextParameter(str));
            N0 = int.Parse(ReadNextParameter(str));
            N_MAX = int.Parse(ReadNextParameter(str));
            str.ReadLine(); str.ReadLine();

            // mutation
            PBm_I = double.Parse(ReadNextParameter(str));
            PDm_I = double.Parse(ReadNextParameter(str));
            PBm_E = double.Parse(ReadNextParameter(str));
            PDm_E = double.Parse(ReadNextParameter(str));
            MI = double.Parse(ReadNextParameter(str));
            ME = double.Parse(ReadNextParameter(str));
            str.ReadLine(); str.ReadLine();

            // fitness
            FE = double.Parse(ReadNextParameter(str));
            FI_A = double.Parse(ReadNextParameter(str));
            FI_B = double.Parse(ReadNextParameter(str));
            str.ReadLine(); str.ReadLine();

            // reproduction
            R = int.Parse(ReadNextParameter(str));
            OFFSPRING = int.Parse(ReadNextParameter(str));
            GENETIC_DRIFT = bool.Parse(ReadNextParameter(str));
            str.ReadLine(); str.ReadLine();
            
            // environment and aging
            ENV = double.Parse(ReadNextParameter(str));
            ENV_FUNCTION = ReadNextParameter(str);
            AGE_FUNCTION = ReadNextParameter(str);
            str.ReadLine(); str.ReadLine();

            // environment check
            T = int.Parse(ReadNextParameter(str));
            PI = double.Parse(ReadNextParameter(str));
            PE = double.Parse(ReadNextParameter(str));
            PD = double.Parse(ReadNextParameter(str));
            POPULATION_BOTTLE = bool.Parse(ReadNextParameter(str));
            str.ReadLine(); str.ReadLine();

            // various
            SEED = int.Parse(ReadNextParameter(str));

            str.Close();
            Console.WriteLine("Input parameters done!");
        }

        // output simulation parameters to a file
        [Obsolete]
        private static void OutputParameters(StreamWriter str)
        {

        }

        // reduce size of array for output
        private static double[,] ReduceArray(double[] arr)
        {
            double[,] redarr = new double[2, 1217];
            int stop = 0;

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (i <= 10 || (i <= 100 && i % 5 == 0) || (i <= 1000 && i % 50 == 0) || (i <= 10000 && i % 100 == 0)
                    || (i <= 100000 && i % 500 == 0) || (i <= 1000000 && i % 1000 == 0))
                {
                    redarr[0, stop] = i;
                    redarr[1, stop] = arr[i];
                    stop++;
                }
            }

            return redarr;
        }

        // output 1D normal arrays to file
        private static void OutputArray(double[] arr, string file)
        {
            StreamWriter s = new StreamWriter(file);

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                s.WriteLine(i + "\t" + arr[i].ToString("0.######"));
            }
            s.Close();
        }

        // output 2D normal arrays to file
        private static void OutputArray(double[,] arr, string file)
        {
            StreamWriter s = new StreamWriter(file);

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arr.GetUpperBound(1); j++)
                {
                    s.Write(arr[i, j].ToString("0.######") + " \t");
                }
                s.WriteLine();
            }
            s.Close();
        }

        // reduces and outputs the reduced arrays to file
        private static void ReduceAndOutputArray(double[] arr, string file)
        {
            double[,] ard = ReduceArray(arr);

            StreamWriter s = new StreamWriter(file);

            s.WriteLine(ard[0, 0] + "\t" + ard[1, 0].ToString("0.######"));
            for (int i = 1; i <= ard.GetUpperBound(1); i++)
            {
                int t = Convert.ToInt32(ard[0, i]);

                if (t > 0)
                {
                    s.WriteLine(t + "\t" + ard[1, i].ToString("0.######"));
                }
            }
            s.Close();
        }

        // helper function to read the next parameter
        private static string ReadNextParameter(StreamReader strValue)
        {
            string s = strValue.ReadLine();
            s = s.Substring(s.LastIndexOf("=") + 1);
            return s;
        }

        // initialize thread priority and input data
        private static void InitializeProgram(string[] argsValue)
        {
            // set thread and process properties
            Process proc = Process.GetCurrentProcess(); // the current process
            Thread thr = Thread.CurrentThread; // the current thread
            thr.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("el-GR"); // set culture for format output
            proc.PriorityClass = ProcessPriorityClass.Idle; // set low priority

            // choose input file
            if (argsValue.Length == 0)
            {
                InputParameters("input.txt"); // input the parameters from the file
            }
            else
            {
                InputParameters(argsValue[0]); // first arguments is file
            }

            // initialize random number generator
            if (SEED == 0) rand = new RandomMT();
            else rand = new RandomMT(SEED);

            // initialize arrays for results
            Nt = new double[TIME + 1]; // population array
            Fia_av = new double[TIME + 1]; // Average Fi_a; 
            Fib_av = new double[TIME + 1]; // Average Fi_b;
            Fe_av = new double[TIME + 1]; // Average Fe;
            Age_av = new double[TIME + 1]; // Average Age; * Age_av[0] = average age of death
            Fia_std = new double[TIME + 1]; // Standard deviation Fi_a; 
            Fib_std = new double[TIME + 1]; // Standard deviation Average Fi_b;
            Fe_std = new double[TIME + 1]; // Standard deviation Average Fe;
            Age_std = new double[TIME + 1]; // Standard deviation Average Age;

            Pa = new double[1002, 56]; // age distribution (first dimension is age, second dimension is time)
            InitializeAgeArray(Pa);
        }

        // initializes the age array
        public static void InitializeAgeArray(double[,] agAr)
        {
            // initialize array (age)
            for (int i = 0; i <= agAr.GetUpperBound(0); i++)
            {
                agAr[i, 0] = i;
            }
            // initialize array (time)
            int stop = 0;
            for (int i = 1; i <= 1000000; i++)
            {
                if (i <= 10 || (i > 10 && i <= 100 && (i % 10 == 0)) || (i > 100 && i <= 1000 && (i % 100 == 0)) || (i > 1000 && i <= 10000 && (i % 1000 == 0))
                    || (i > 10000 && i <= 100000 && (i % 10000 == 0)) || (i > 100000 && i <= 1000000 && (i % 100000 == 0)))
                {
                    stop++;
                    agAr[0, stop] = i;
                }
            }
        }

        // normalizes the average-std and population arrays
        private static void Normalize_Pop_Avg_Std()
        {
            for (int i = 1; i <= Nt.GetUpperBound(0); i++)
            {
                Nt[i] /= RUNS; Fia_av[i] /= RUNS; Fib_av[i] /= RUNS; Fe_av[i] /= RUNS; Age_av[i] /= RUNS;
                Fia_std[i] /= RUNS; Fib_std[i] /= RUNS; Fe_std[i] /= RUNS; Age_std[i] /= RUNS;
            }
        }
    }
}
