﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kage
{
    class Habitat
    {
        // variables
        private List<Animal> animals;
        public static int DriftDeaths = 0;
        public static int AgeDeaths = 0;
        public static int EnvironmentDeaths = 0;
        public static double DriftDeathAge = 0.0;
        public static double AgeDeathAge = 0.0;
        public static double EnvironmentDeathAge = 0.0;

        // helper variables
        public int timeAge;

        // properties
        public int N { get { return Animals.Count; } }
        public List<Animal> Animals { get { return animals; } }

        // constructors
        public Habitat()
        {
            animals = new List<Animal>(Program.N0);
        }
        
        // removal of animals due to random events with probability p
        [Obsolete]
        private void RemoveRandomAnimals(double p)
        {
            int nm = N;
            int i = 0; // counter for traversing the animals

            while (i < nm)
            {
                Animal an = Animals[i];

                if (Program.rand.NextDouble() <= p)
                {
                    Animals.Remove(an);
                    i--;
                    nm--;
                }
                i++; // next animal
            } 
        }

        // populates the habitat with animals
        public void Populate(int pop)
        {
            for (int i = 0; i < pop; i++)
            {
                animals.Add(new Animal());
            }
        }

        // randomizes the genome of animals
        public void RandomizeIntrinsicFitness(bool normalDistribution)
        {
            if (normalDistribution) // Normal Distribution
            {
                int imut = 10; // mutation  number for intrinsic fitness
                double imut_step = 0.001; // mutation  step for intrinsic fitness

                foreach (Animal a in Animals)
                {
                    // intrinsic fitness randomization
                    for (int i = 0; i < imut; i++)
                    {
                        if (Program.rand.NextDouble() < 0.5) a.Fi_a += imut_step;
                        else a.Fi_a -= imut_step;
                    }

                    // also randomize b
                    if (!Program.B_CONST)
                    {
                        if (Program.rand.NextDouble() < 0.5) a.Fi_b += imut_step;
                        else a.Fi_b -= imut_step;
                    }
                }
            }
            else
            {
                foreach (Animal a in Animals) // uniform distribution
                {
                    a.Fi_a = Program.rand.NextDouble() * Program.FI_A; // intrinsic fitness randomization
                    if (!Program.B_CONST) a.Fi_b = Program.rand.NextDouble() * Program.FI_B; // also randomize b
                }
            }
        }

        // randomizes the genome of animals
        public void RandomizeExtrinsicFitness(bool normalDistribution)
        {
            if (normalDistribution) // Normal Distribution
            {
                int emut = 10; // mutation  number for extrinsic fitness
                double emut_step = 0.001; // mutation  step for extrinsic fitness

                foreach (Animal a in Animals)
                {
                    // extrinsic fitness randomization
                    for (int i = 0; i < emut; i++)
                    {
                        if (Program.rand.NextDouble() < 0.5) a.Fe += emut_step;
                        else a.Fe -= emut_step;
                    }
                }
            }
            else
            {
                foreach (Animal a in Animals) // uniform distribution
                {
                    a.Fe = Program.rand.NextDouble() * Program.FE; // extrinsic fitness randomization
                }
            }
        }

        // sets initial age equal to reproductive age
        public void SetInitialAge(int inAge)
        {
            foreach (Animal an in Animals)
            {
                an.IncreaseAge(inAge);
            }
        }

        // executes one time step of the simulation (time is the current time step)
        public void ExecuteTimeStep(int time)
        {
            int nm = N;
            int i = 0; // counter for traversing the animals

            // can catastrophic events occur? if yes, they occur every BOTTLE_TIME time steps
            if (Program.POPULATION_BOTTLE && (time % Program.BOTTLE_TIME == 0))
            {
                while (i < nm)
                {
                    Animal an = Animals[i];

                    // check if animal if alive
                    if (Program.rand.NextDouble() < Program.PD) // animal dies
                    {
                        an.Status = AnimalStatus.DriftDeath;
                        Animals.Remove(an);
                        i--;
                        nm--;
                        Habitat.DriftDeaths++;
                        Habitat.DriftDeathAge += an.Age;
                    }
                    i++;
                }
            }

            nm = N; i = 0; // reset values

            double[] na = new double[nm], nb = new double[nm], nfe = new double[nm], nage = new double[nm]; // destribution arrays
            double av_a = 0.0, av_b = 0.0, av_fe = 0.0, av_age = 0.0; // average values
            int a = 0; // counter for each animal

            // will I update age distribution?
            bool calcAgeDist = time <= 10 || (time > 10 && time <= 100 && (time % 10 == 0)) || (time > 100 && time <= 1000 && (time % 100 == 0)) || (time > 1000 && time <= 10000 && (time % 1000 == 0)) 
                    || (time > 10000 && time <= 100000 && (time % 10000 == 0)) || (time > 100000 && time <= 1000000 && (time % 100000 == 0));
            if (calcAgeDist) timeAge++; // program will stop stopped to calculate distribution

            while (i < nm)
            {
                Animal an = Animals[i];
                an.IncreaseAge(); // increase age

                // update the average values and helping distributions (to calculate stds later)
                na[a] = an.Fi_a; nb[a] = an.Fi_b; nfe[a] = an.Fe; nage[a] = an.Age;
                av_a += an.Fi_a; av_b += an.Fi_b; av_fe += an.Fe; av_age += an.Age;

                // update the corresponding age distribution
                if (calcAgeDist) // program will stop to calculate age distribution
                {
                    if (an.Age <= 1000)
                    {
                        Program.Pa[an.Age, timeAge]++;
                    }
                    else
                    {
                        Program.Pa[1001, timeAge]++;
                    }
                }

                // 1. check random drift
                if (!Program.POPULATION_BOTTLE && Program.rand.NextDouble() < Program.PD && an.Status == AnimalStatus.Alive)
                {
                    an.Status = AnimalStatus.DriftDeath; // animal dies from random causes
                    Habitat.DriftDeaths++;
                    Habitat.DriftDeathAge += an.Age;
                }

                double r = Program.rand.NextDouble(); // random number to check for mutations

                // 2. check intrinsic fitness
                if (r < Program.PI && an.Status == AnimalStatus.Alive)
                {
                    // aging check
                    if (Program.rand.NextDouble() > an.CalcFi())
                    {
                        an.H += 1;
                        if (an.H >= Program.T)
                        {
                            an.Status = AnimalStatus.AgeDeath; // animal dies from aging
                            Habitat.AgeDeaths++;
                            Habitat.AgeDeathAge += an.Age;
                        }
                    }
                }

                // 3. check extrinsic fitness
                if (r < Program.PE && an.Status == AnimalStatus.Alive)
                {
                    if (Program.ENV_COMPATIBLE) // is an animal tested to how compatible is to the environment?
                    {
                        // check how well animal is adapted to environment
                        double envComp = Math.Abs(CalculateEnvironment(time, Program.ENV_PERIOD) - an.Fe);

                        // environmental check
                        if (Program.rand.NextDouble() <= envComp)
                        {
                            an.H += 1;
                            if (an.H >= Program.T)
                            {
                                an.Status = AnimalStatus.EnvironmentDeath; // animal dies from environment
                                Habitat.EnvironmentDeaths++;
                                Habitat.EnvironmentDeathAge += an.Age;
                            }
                        }

                    }
                    else // fittest animal is the one with an.Fe == 1
                    {
                        // environmental check
                        if (Program.rand.NextDouble() > an.Fe)
                        {
                            an.H += 1;
                            if (an.H >= Program.T)
                            {
                                an.Status = AnimalStatus.EnvironmentDeath; // animal dies from environment
                                Habitat.EnvironmentDeaths++;
                                Habitat.EnvironmentDeathAge += an.Age;
                            }
                        }
                    }
                }

                // check if animal if alive
                if (an.Status != AnimalStatus.Alive) // animal is dead
                {
                    Animals.Remove(an);
                    i--;
                    nm--;
                }
                else if (an.Age >= Program.R) // animal lives and reproduces according to age
                {
                    int realOffspring = Program.OFFSPRING;

                    if (Program.GENETIC_DRIFT) // genetic drift works on number of offspring, else constant number of offspring is produced
                    {
                        realOffspring = Program.rand.Next(Program.MIN_OFFSPRING, Program.OFFSPRING + 1);
                    }

                    for (int j = 1; j <= realOffspring; j++)
                    {
                        Animal child = an.Reproduce();

                        if (Program.rand.NextDouble() > (double)N / Program.N_MAX) // check Verhulst
                        {
                            Animals.Add(child);
                        }
                    }
                }

                a++;
                i++; // next animal
            }

            // update the population array
            Program.Nt[time] += N;

            // update the average values and calculate standard deviation (a now is the total number of animals "read")
            av_a /= a; av_b /= a; av_fe /= a; av_age /= a; // update average values
            Program.Fia_av[time] += av_a; Program.Fib_av[time] += av_b; Program.Fe_av[time] += av_fe; Program.Age_av[time] += av_age; // update average arrays

            // calculate standard deviations
            double[][] dst = new double[4][];
            dst[0] = na;  dst[1] = nb; dst[2] = nfe; dst[3] = nage;
            double[] avs = new double[] { av_a, av_b, av_fe, av_age };
            double[] stds = NMath.CalculateStandardDeviation(dst, avs);
            Program.Fia_std[time] += stds[0]; Program.Fib_std[time] += stds[1]; Program.Fe_std[time] += stds[2]; Program.Age_std[time] += stds[3]; // update standard deviation arrays

            //TODO: . check if average and standard deviations and age distributions are calculated correctly
        }

        // the current environment of the habitat
        public double CalculateEnvironment(double a, double x)
        {
            // set parameter for each simulation
            double b = 0.99;

            switch (Program.ENV_FUNCTION)
            {
                case "constant":
                    return Program.ENV;
                case "linear":
                    return NMath.Linear(a, b, x);
                case "logistic":
                    return NMath.Logistic(a, b, x);
                case "exponential":
                    return NMath.Exponential(a, b, x);
                case "powerlaw":
                    return NMath.PowerLaw(a, b, x);
                case "sine":
                    return NMath.SinEnv(a, x);
                case "cosine":
                    return NMath.CosEnv(a, x);
                default:
                    throw new Exception("Invalid Environment Function");
            }
        }
    }

    class Animal
    {
        // variables
        private int age; // the age of the animal
        private int h; // the number of "hits"
        private double a; // denotes the first parameter of the aging function (i.e y = ax + b). a and b determing the intrinsic fitness fi
        private double b; // denotes the second parameter of the aging function (i.e y = ax + b) 
        private double fe; // extrinsic fitness (environment dependent)
        private AnimalStatus status; // how did the animal die?

        // properties
        public int Age { get { return age; } }
        public double Fi_a // changes the parameter that affects the intrinsic fitness (usually due to a mutation)
        {
            get
            {
                return a;
            }
            set
            {
                if ((value >= Program.MI && value > 0) || Program.A_NEG) // accept new value if Fi is within accepted limits (or negative values are allowed)
                {
                    a = value; // accept new value
                }
            }
        }
        public double Fi_b 
        {
            get 
            {
                return b; 
            }
            set 
            {
                if (value > 0 && value < 1)
                {
                    b = value;
                }
            }
        }
        public double Fe // changes the parameter that affects the extrinsic fitness (usually due to a mutation)
        {
            get
            {
                return fe;
            }
            set
            {
                if (value >= 0 && value <= 1) fe = value; // accept new value if Fe is within accepted limits
            }
        }
        public int H
        {
            get
            {
                return h;
            }
            set
            {
                h = value;
                if (h < 0) throw new Exception("Invalid H value"); // check for correct H value
            }
        }
        public AnimalStatus Status { get { return status; } set { status = value; } } // is the animal alive or how did it die?

        // constructors
        public Animal()
        {
            age = 0;
            h = 0;
            fe = Program.FE;
            a = Program.FI_A;
            b = Program.FI_B;
            status = AnimalStatus.Alive;
        }

        public Animal(double feValue, double fiaValue, double fibValue)
        {
            age = 0;
            h = 0;
            fe = feValue;
            a = fiaValue;
            b = fibValue;
            status = AnimalStatus.Alive;
        }

        // returns the current age dependent (intrinsic fitness) of the animal
        public double CalcFi()
        {
            switch (Program.AGE_FUNCTION)
            {
                case "linear":
                    return NMath.Linear(Fi_a, Fi_b, Age);
                case "logistic":
                    return NMath.Logistic(Fi_a, Fi_b, Age);
                case "exponential":
                    return NMath.Exponential(Fi_a, Fi_b, Age);
                case "powerlaw":
                    return NMath.PowerLaw(Fi_a, Fi_b, Age);
                default:
                    throw new Exception("Invalid Aging Function");
            }
        }

        // helps to check the intrinsic fitness of an animal with given paramaters and ensure it is 0 <= CalcFi() <= 1
        public static double CalcFi(double a, double b, int age)
        {
            switch (Program.AGE_FUNCTION)
            {
                case "linear":
                    return NMath.Linear(a, b, age);
                case "logistic":
                    return NMath.Logistic(a, b, age);
                case "exponential":
                    return NMath.Exponential(a, b, age);
                case "powerlaw":
                    return NMath.PowerLaw(a, b, age);
                default:
                    throw new Exception("Invalid Aging Function");
            }
        }

        // mutate, change Fe, Fi (Program.B_CONST=true means b stays constant)
        public void Mutate()
        {
            double r = 0.0;
            double MIb = Program.MI;

            // **NOTE** how a and fe changes depends a lot on aging function (current settings are for logistic function)

            // change intrinsic fitness
            r = Program.rand.NextDouble();
            if (r <= Program.PBm_I) // beneficial mutation
            {
                Fi_a -= Program.MI;
            }
            else if (r > Program.PBm_I && r < (Program.PBm_I + Program.PDm_I)) // dentrimental mutation 
            {
                Fi_a += Program.MI;
            }

            if (!Program.B_CONST)
            {
                // change intrinsic fitness for b
                r = Program.rand.NextDouble();
                if (r <= Program.PBm_I) // beneficial mutation
                {
                    Fi_b += MIb;
                }
                else if (r > Program.PBm_I && r < (Program.PBm_I + Program.PDm_I)) // dentrimental mutation 
                {
                    Fi_b -= MIb;
                }
            }

            // change extrinsic fitness
            r = Program.rand.NextDouble();
            if (r <= Program.PBm_E) // beneficial mutation
            {
                Fe += Program.ME;
            }
            else if (r > Program.PBm_E && r < (Program.PBm_E + Program.PDm_E)) // dentrimental mutation 
            {
                Fe -= Program.ME;
            }
        }

        // reproduction
        public Animal Reproduce()
        {
            Animal child = new Animal(Fe, Fi_a, Fi_b); // create new animal
            child.Mutate(); // do the mutations
            return child;
        }

        // age increase, return current age
        public void IncreaseAge()
        {
            IncreaseAge(1);
        }

        // increase age a specific number of years, mainly used for program initialization
        public void IncreaseAge(int ag)
        {
            // check alive
            if (Status == AnimalStatus.Alive) age += ag;
            else throw new Exception("Trying to increase age of dead animal");

            // check for maximum age
            if (Age > Program.MAX_AGE) age = Program.MAX_AGE;
        }

    }

    // describes the way the animal died (last hit), or if it is alive
    enum AnimalStatus
    { AgeDeath, EnvironmentDeath, DriftDeath, Alive }

    public class RandomMT : NPack.MersenneTwister
    {
        #region Base Class Constructor Calls (added by Telis)

        public RandomMT() : base() { }

        public RandomMT(Int32 seed) : base(seed) { }

        public RandomMT(Int32[] initKey) : base(initKey) { }

        #endregion

        #region Custom readonly Constants (Added by telis)

        public readonly double Y_MIN, Y_MAX, G; // readonly constants added for power law distribution

        #endregion

        #region Custom Functions (Added by telis)

        // custom constructor

        public RandomMT(double ymin, double ymax, double gValue)
            : base()
        {
            Y_MIN = ymin;
            Y_MAX = ymax;
            G = gValue;
        }

        public RandomMT(double ymin, double ymax, double gValue, int seed)
            : base(seed)
        {
            Y_MIN = ymin;
            Y_MAX = ymax;
            G = gValue;
        }

        public double NextPowDouble() // returns the next double precision random number with power law distribution
        {
            double gp = 1.0 + G;
            double expr = (Math.Pow(Y_MAX, gp) - Math.Pow(Y_MIN, gp)) * NextDouble() + Math.Pow(Y_MIN, gp);
            return Math.Pow(expr, 1.0 / gp);
        }

        public int NextPow() // returns the next random integer with power law distribution
        {
            return (int)NextPowDouble();
        }

        #endregion
    }

    public static class NMath // additional math functions
    {
        public static double Linear(double a, double b, double x)
        {
            return (-a * x + b); // y = -ax + b
        }

        public static double Logistic(double a, double b, double x)
        {
            return (1 / (1 + (1 / b - 1) * Math.Exp(a * x))); // y = 1 / (1+(1/b-1)*exp(ax))
        }

        public static double Exponential(double a, double b, double x)
        {
            return (b * Math.Exp(-a * x)); // y = b * exp(ax)
        }

        public static double PowerLaw(double a, double b, double x)
        {
            return (b * Math.Pow(x, a)); // y = b * x^a
        }

        public static double Factorial(int a) // return the factorial of a number (a must be >=1)
        {
            if (a == 1)
            {
                return 1;
            }
            else
            {
                return a * (Factorial(a - 1));
            }
        }

        public static double SinEnv(double a, double x) // enviromental periodic function base on sine
        {
            return (Math.Sin(x / (a * Math.PI)) + 1) / 2;
        }

        public static double CosEnv(double a, double x)  // enviromental periodic function base on cosine
        {
            return (Math.Cos(x / (a * Math.PI)) + 1) / 2;
        }

        // Normalizes given distribution
        public static void NormalizeDistribution(double[] data)
        {
            double N = 0;

            for (int i = 0; i <= data.GetUpperBound(0); i++)
            {
                N += data[i];
            }
            for (int i = 0; i <= data.GetUpperBound(0); i++)
            {
                data[i] /= N;
            }
        }

        // Normalizes given distribution
        public static double[] NormalizeDistribution(int[] data)
        {
            int N = 0;
            double[] normData = new double[data.GetUpperBound(0) + 1];

            for (int i = 0; i <= data.GetUpperBound(0); i++)
            {
                N += data[i];
                normData[i] = data[i];
            }
            for (int i = 0; i <= data.GetUpperBound(0); i++)
            {
                normData[i] /= N;
            }

            return normData;
        }

        // Normalize age distribution array
        public static double[,] NormalizeAgeDistribution(double[,] ageData)
        {
            double[,] normData = new double[ageData.GetUpperBound(0) + 1, ageData.GetUpperBound(1) + 1];
            
            Program.InitializeAgeArray(normData);

            for (int i = 1; i <= ageData.GetUpperBound(1); i++)
            {
                double n = 0.0;

                for (int j = 1; j <= ageData.GetUpperBound(0); j++)
                {
                    n += ageData[j, i];
                }
                for (int j = 1; j <= ageData.GetUpperBound(0); j++)
                {
                    normData[j, i] = ageData[j, i] / n;
                }
            }

            return normData;
        }

        // calculates the cumulative age distribution given the normalized age distribution (survival probability)
        public static double[,] CalculateCumulativeAgeDistribution(double[,] ageData)
        {
            double[,] normData = new double[ageData.GetUpperBound(0) + 1, ageData.GetUpperBound(1) + 1];

            Program.InitializeAgeArray(normData);

            for (int i = 1; i <= ageData.GetUpperBound(1); i++)
            {
                double f = 0.0;

                for (int j = 1; j <= ageData.GetUpperBound(0); j++)
                {
                    normData[j, i] = 1.0 - f;
                    f += ageData[j, i];
                }
            }

            return normData;
        }

        // calculates the mortality function given the corresponding survival probability
        public static double[,] CalculateMortalityFunction(double[,] survData)
        {
            double[,] normData = new double[survData.GetUpperBound(0) + 1, survData.GetUpperBound(1) + 1];

            Program.InitializeAgeArray(normData);

            for (int i = 1; i <= survData.GetUpperBound(1); i++)
            {
                for (int j = 1; j < survData.GetUpperBound(0); j++)
                {
                    normData[j, i] = Math.Log(survData[j, i] / survData[j + 1, i]);
                }
            }

            return normData;
        }
        
        // calculates average (distribution should be normalized)
        public static double CalculateAverage(double[] data, bool distribution)
        {
            double av = 0.0;

            if (distribution)
            {
                for (int i = 0; i <= data.GetUpperBound(0); i++)
                {
                    av += i * data[i]; // i represents x value of the distribution
                }
                return av;
            }
            else
            {
                for (int i = 0; i <= data.GetUpperBound(0); i++)
                {
                    av += data[i];
                }
                av /= data.GetUpperBound(0) + 1;
                return av;
            }
        }

        // calculates standard deviation (distribution should be normalized), average value is given
        public static double CalculateStandardDeviation(double[] data, double av, bool distribution)
        {
            double std = 0.0;

            if (distribution)
            {
                for (int i = 0; i <= data.GetUpperBound(0); i++)
                {
                    std += (data[i] * Math.Pow((double)i - av, 2.0)); // i represents x value of the distribution
                }
                return Math.Sqrt(std);
            }
            else
            {
                for (int i = 0; i <= data.GetUpperBound(0); i++)
                {
                    std += (Math.Pow(data[i] - av, 2.0));
                }
                std /= data.GetUpperBound(0) + 1;
                return Math.Sqrt(std);
            }
        }

        // calculates standard deviation (distribution should be normalized)
        public static double CalculateStandardDeviation(double[] data, bool distribution)
        {
            return CalculateStandardDeviation(data, CalculateAverage(data, distribution), distribution);
        }

        // calculates standard deviation of a number (4) of arrays (Fi_a, Fi_b, Fe, Age), data must NOT be in distribution form, array sizes MUST be equal
        public static double[] CalculateStandardDeviation(double[][] data, double[] av)
        {
            double[] ret = new double[4]; // return array

            double std_a = 0.0; double std_b = 0.0; double std_fe = 0.0; double std_age = 0.0;

            for (int i = 0; i <= data[0].GetUpperBound(0); i++)
            {
                std_a += (Math.Pow(data[0][i] - av[0], 2.0)); std_b += (Math.Pow(data[1][i] - av[1], 2.0));
                std_fe += (Math.Pow(data[2][i] - av[2], 2.0)); std_age += (Math.Pow(data[3][i] - av[3], 2.0));
            }

            std_a /= data[0].GetUpperBound(0) + 1; std_b /= data[1].GetUpperBound(0) + 1;
            std_fe /= data[2].GetUpperBound(0) + 1; std_age /= data[3].GetUpperBound(0) + 1;

            std_a = Math.Sqrt(std_a); std_b = Math.Sqrt(std_b); std_fe = Math.Sqrt(std_fe); std_age = Math.Sqrt(std_age);
            ret[0] = std_a; ret[1] = std_b; ret[2] = std_fe; ret[3] = std_age;

            return ret;
        }
    }
}
